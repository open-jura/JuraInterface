/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include "JuraObfuscation.h"

const static uint8_t ESC = 0x1b;

uint8_t JuraObfuscator::deobfuscate_a_nibble(uint8_t nib, uint8_t upper_key,
		uint8_t lower_key, uint8_t counter) const
{
	uint8_t first, second, third;
	uint8_t mod_ct = (counter >> 4);

	first = primary[nib + upper_key + counter] - upper_key - counter;

	second = secondary[first + lower_key + mod_ct] - lower_key - mod_ct;

	third = primary[second + upper_key + counter] - upper_key - counter;

	return third & 0xf;
}

uint8_t JuraObfuscator::obfuscate_a_nibble(uint8_t nib, uint8_t upper_key,
		uint8_t lower_key, uint8_t counter) const
{
	uint8_t second, first, zeroth;
	uint8_t mod_ct = (counter >> 4);

	second = primary[nib + upper_key + counter] - upper_key - counter;

	first = secondary[second + lower_key + mod_ct] - lower_key - mod_ct;

	zeroth = primary[first + upper_key + counter] - upper_key - counter;

	return zeroth & 0xf;
}

struct inbuf
{
	const uint8_t* in;
	uint8_t index = 0;

	inbuf(const uint8_t* in): in(in) {}

	uint8_t pop()
	{
		uint8_t ch = in[index++];
		if (ch == ESC) {
			ch = in[index++] ^ 0x80;
		}
		return ch;
	}
};

void JuraObfuscator::deobfuscate(uint8_t* out, const uint8_t* in) const
{
	inbuf ib(in);

	uint8_t keychar = ib.pop();

	uint8_t lower_key = keychar & 0xf;
	uint8_t upper_key = keychar >> 4;

	uint8_t out_counter = 0;
	uint8_t obf_counter = 0;
	uint8_t c;
	while ((c = ib.pop()) != '\0') {
		if (c == '\r')
			break;

		uint8_t upper_nib = c >> 4;
		uint8_t lower_nib = c & 0xf;

		uint8_t deoc = deobfuscate_a_nibble(
				upper_nib, upper_key, lower_key, obf_counter) << 4;
		deoc |= deobfuscate_a_nibble(
				lower_nib, upper_key, lower_key, obf_counter + 1);
		obf_counter += 2;
		out[out_counter++] = deoc;
	}
	out[out_counter] = '\0';
}

struct outbuf {
	uint8_t* out;
	uint8_t index = 0;

	outbuf(uint8_t* out): out(out) {}

	void write(uint8_t val)
	{
		if (val == 0 || val == '\r' || val == '\n' || val == '&' || val == ESC) {
			// character is special and needs escape
			out[index++] = ESC;
			val ^= 0x80;
		}
		out[index++] = val;
	}

	void end()
	{
		out[index++] = '\0';
	}
};

void JuraObfuscator::obfuscate(uint8_t* out, const uint8_t* in, uint8_t key) const
{
	outbuf ob(out);

	ob.write(key);

	uint8_t lower_key = key & 0xf;
	uint8_t upper_key = key >> 4;

	uint8_t obf_counter = 0;
	uint8_t in_index = 0;
	uint8_t c;
	while ((c = in[in_index++]) != 0) {
		uint8_t upper_nib = c >> 4;
		uint8_t lower_nib = c & 0xf;

		uint8_t obf = obfuscate_a_nibble(
				upper_nib, upper_key, lower_key, obf_counter) << 4;
		obf |= obfuscate_a_nibble(
				lower_nib, upper_key, lower_key, obf_counter + 1);
		obf_counter += 2;
		ob.write(obf);
	}
	ob.end();
}
