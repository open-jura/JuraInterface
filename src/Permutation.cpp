/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#include "Permutation.h"

const uint8_t arr1[16] = {0x8, 0xE, 0xC, 0x4, 0x3, 0xD, 0xA, 0xB, 0x0, 0xF, 0x6, 0x7, 0x2, 0x5, 0x1, 0x9};
const uint8_t arr2[16] = {0x4, 0xB, 0xD, 0xA, 0x0, 0x7, 0xF, 0x5, 0x9, 0x8, 0x3, 0x1, 0xE, 0x2, 0xC, 0x6};
const uint8_t arr3[16] = {0xE, 0x4, 0x3, 0x2, 0x1, 0xD, 0x8, 0xB, 0x6, 0xF, 0xC, 0x7, 0xA, 0x5, 0x0, 0x9};
const uint8_t arr4[16] = {0xA, 0x6, 0xD, 0xC, 0xE, 0xB, 0x1, 0x9, 0xF, 0x7, 0x0, 0x5, 0x3, 0x2, 0x4, 0x8};

const Permutation Permutation::P1(arr1);
const Permutation Permutation::P2(arr2);
const Permutation Permutation::P3(arr3);
const Permutation Permutation::P4(arr4);

