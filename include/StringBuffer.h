/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#ifndef STRING_BUFFER_H
#define STRING_BUFFER_H

#include <stddef.h>

class StringBuffer {
	/*
	 * Fixed-size char buffer class, has less fragmentation issues
	 * when heap is limited.
	 */
public:
	/* create buffer with maximum size length */
	StringBuffer(size_t length);

	/* copy null-terminated string length of the StringBuffer will be
	 * length of the string (+1 for \0)*/
	StringBuffer(const char* copy);

	/* deep copy */
	StringBuffer(const StringBuffer& copy);

	/* move */
	StringBuffer(StringBuffer&& rhs);

	virtual ~StringBuffer();

	/* copy null-terminated string */
	bool assign(const char*);

	/* append character */
	bool put(char c);

	/* append nul-terminated string */
	bool append(const char* c);
	bool append(const StringBuffer& c);

	/* convert number to hex and append. lenth: field length in number of bytes
	 */
	void appendHex(unsigned long long i, size_t length);

	/* is string terminated by "\r\n"? */
	bool isTerminated() const;

	/* get pointer to buffer */
	char* get();
	const char* get() const;

	/* if buffer was modified, reset the index to the NUL, so that put works
	 * again */
	void relocateIndex();

	/* set to empty string */
	void clear();

	/* remove last character */
	char pop();

	/* access by index */
	char& operator[](size_t index);
	char operator[](size_t index) const;

private:
	const size_t length;
	char* buffer;
	size_t index;

};

#endif

