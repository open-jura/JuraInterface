/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */

#ifndef JURA_COMM_INTERFACE
#define JURA_COMM_INTERFACE

#include "StringBuffer.h"
#include <stddef.h>
#include <stdint.h>

class IJuraSerial;
class JuraObfuscator;

class JuraMessageView
{
	StringBuffer buffer;
	char* commandStart;
	char* parameterStart;
	char* secondParameterStart;
	bool hasFirstPar;
	bool hasSecondPar;
	bool obfuscationOn;

	void init();
	void initDeobfuscate(const JuraObfuscator&);
public:
	/* Parse buffer and split into protocol type, command, and first/second
	 * parameter. The format is:
	 * [protocol type char]command[`:`first parameter[`,`second parameter]
	 *
	 * If a JuraObfuscator is given and the first char of the bufer is '&',
	 * the buffer will be deobfuscated and the obfuscated flag will be set.
	 */
	JuraMessageView(const char* buffer);
	JuraMessageView(const StringBuffer& buffer);
	JuraMessageView(const char* buffer, const JuraObfuscator& obfuscator);
	JuraMessageView(const StringBuffer& buffer, const JuraObfuscator& obfuscator);
	JuraMessageView(JuraMessageView&&) = default;

	/* Return if message is new protocol, i.e. starts with `@` */
	bool isNewProtocol() const;

	bool hasFirstParameter() const;

	bool hasSecondParameter() const;

	bool isObfuscated() const;

	const char* getCommand() const;

	/* Note that a message can have empty first or second parameters */
	const char* getFirstParameter() const;
	const char* getSecondParameter() const;
};

class JuraInterface
{
	IJuraSerial& serial;
	JuraObfuscator& obfuscator;
	
	StringBuffer buffer;

	bool updateSerial();

public:
	JuraInterface(IJuraSerial& serial, JuraObfuscator& obfuscator);
	virtual ~JuraInterface() = default;

	void sendPlain(const char* message);

	/* max length of obfuscated message: 253 */
	void sendObfuscated(const char* message);

	void send(const char* message, bool obfuscated);

	void update();

	bool messageAvailable() const;

	void copyMessage(char* into, size_t num);
	JuraMessageView getMessage();

	void clearMessage();
};

#endif // JURA_COMM_INTERFACE

