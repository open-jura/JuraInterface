/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#ifndef JURAOBFUSCATION_H
#define JURAOBFUSCATION_H

#include <stdint.h>
#include "Permutation.h"

class JuraObfuscator
{
	const Permutation& primary;
	const Permutation& secondary;

	uint8_t deobfuscate_a_nibble(uint8_t nib, uint8_t upper_key,
			uint8_t lower_key, uint8_t counter) const;

	uint8_t obfuscate_a_nibble(uint8_t nib, uint8_t upper_key,
			uint8_t lower_key, uint8_t counter) const;

public:
	/*
	 * For Jura E8, choose primary = Permutation::P1, secondary =
	 * Permutation::P2
	 */
	JuraObfuscator(const Permutation& primary, const Permutation& secondary):
		primary(primary), secondary(secondary)
	{};

	/*
	 * Deobfuscate a byte string.
	 * in must start with the key char (first char after the '&').
	 * deobfuscate will stop if it encounters '\0' or '\r'.
	 * out must be a buffer at least same size as in.
	 * deobfuscate works inplace, so you can call deobfuscate(in, in);
	 */
	void deobfuscate(uint8_t* out, const uint8_t* in) const;

	/*
	 * Obfuscate a byte string using the given key char.
	 * Will output a string starting with the key char.
	 *
	 * out must be a buffer able to contain at least
	 * 2 * length(in) + 3 bytes
	 */
	void obfuscate(uint8_t* out, const uint8_t* in, uint8_t key) const;
};

#endif // JURAOBFUSCATION_H

