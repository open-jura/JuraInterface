/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#ifndef JURAOBFUSCATION_PERMUTATION_H
#define JURAOBFUSCATION_PERMUTATION_H

#include <stdint.h>

class Permutation
{
	const uint8_t* data;

public:
	Permutation(const uint8_t* data): data(data) {};

	uint8_t operator [](uint8_t loc) const
	{
		return data[loc % 16];
	}

	const static Permutation P1;
	const static Permutation P2;
	const static Permutation P3;
	const static Permutation P4;
};

#endif // JURAOBFUSCATION_PERMUTATION_H

